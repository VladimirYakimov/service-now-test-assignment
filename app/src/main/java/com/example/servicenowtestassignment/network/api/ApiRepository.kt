package com.example.servicenowtestassignment.network.api

import androidx.lifecycle.LiveData
import com.example.servicenowtestassignment.view.mainview.ProgressState

interface ApiRepository {
    suspend fun getJoke(firstName: String, lastName: String)
    fun getProgressLiveData(): LiveData<ProgressState>
    fun getJokeLiveData(): LiveData<List<String>>
}