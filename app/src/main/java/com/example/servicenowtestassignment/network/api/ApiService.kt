package com.example.servicenowtestassignment.network.api

import com.example.servicenowtestassignment.db.JokeEntity
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("jokes/random")
    suspend fun getRandomJoke(
        @Query("firstName") firstName: String,
        @Query("lastName") lastName: String,
        @Query("limitTo") limitTo: Array<Any> = arrayOf("nerdy")
    ): RandomJokeModel

    data class RandomJokeModel(
        val type: String,
        val value: JokeEntity
    )

    data class Joke(
        val categories: List<String>,
        val id: Int,
        val joke: String
    )
}