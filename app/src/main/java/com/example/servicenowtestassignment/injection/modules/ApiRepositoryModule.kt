package com.example.servicenowtestassignment.injection.modules

import com.example.servicenowtestassignment.db.JokeDao
import com.example.servicenowtestassignment.db.JokeRoomApiRepository
import com.example.servicenowtestassignment.network.api.ApiRepository
import com.example.servicenowtestassignment.network.api.ApiService
import dagger.Module
import dagger.Provides

@Module
class ApiRepositoryModule {

    @Provides
    fun provideApiRepository(apiService: ApiService, jokeDao: JokeDao): ApiRepository {
        return JokeRoomApiRepository(apiService, jokeDao)
    }
}