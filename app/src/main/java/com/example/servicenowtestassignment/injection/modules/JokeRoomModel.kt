package com.example.servicenowtestassignment.injection.modules

import android.content.Context
import androidx.room.Room
import com.example.servicenowtestassignment.db.JokeDao
import com.example.servicenowtestassignment.db.JokeDataBase
import dagger.Module
import dagger.Provides

@Module
class JokeRoomModel {

    @Provides
    fun provideJokeRoom(context: Context): JokeDataBase {
        return Room.databaseBuilder(
            context,
            JokeDataBase::class.java, "joke-database"
        ).build()
    }

    @Provides
    fun provideJokeDao(jokeDataBase: JokeDataBase): JokeDao {
        return jokeDataBase.jokeDao()
    }
}