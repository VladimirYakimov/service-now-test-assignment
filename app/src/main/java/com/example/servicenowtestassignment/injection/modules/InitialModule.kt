package com.example.servicenowtestassignment.injection.modules

import android.content.Context
import android.content.SharedPreferences
import dagger.Module
import dagger.Provides

const val FIRST_NAME_KEY = "FIRST_NAME_KEY"
const val LAST_NAME_KEY = "LAST_NAME_KEY"

@Module
class InitialModule {
    @Provides
    fun provideSharedPreferences(context: Context): SharedPreferences =
        context.getSharedPreferences("SharedPreferences", Context.MODE_PRIVATE)

}