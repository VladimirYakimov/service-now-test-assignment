package com.example.servicenowtestassignment.injection.modules

import com.example.servicenowtestassignment.view.initial.InitialActivity
import com.example.servicenowtestassignment.view.mainview.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class AppBindingModule {
    @ContributesAndroidInjector
    abstract fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector
    abstract fun bindInitialActivity(): InitialActivity
}