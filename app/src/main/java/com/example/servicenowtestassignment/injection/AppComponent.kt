package com.example.servicenowtestassignment.injection

import android.app.Application
import com.example.servicenowtestassignment.injection.modules.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [AndroidSupportInjectionModule::class, AppModule::class,
        AppBindingModule::class, RetrofitModule::class, ViewModelModule::class, InitialModule::class,
        ApiRepositoryModule::class, JokeRoomModel::class]
)
interface AppComponent : AndroidInjector<DaggerApplication> {
    override fun inject(app: DaggerApplication)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }
}