package com.example.servicenowtestassignment.db

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.servicenowtestassignment.network.api.ApiRepository
import com.example.servicenowtestassignment.network.api.ApiService
import com.example.servicenowtestassignment.view.mainview.ProgressState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class JokeRoomApiRepository(private val apiService: ApiService, private val jokeDao: JokeDao) :
    ApiRepository {
    private val TAG = "RoomApiRepository"

    private val progressLiveData = MutableLiveData<ProgressState>()

    override suspend fun getJoke(firstName: String, lastName: String) {
        withContext(Dispatchers.IO) {
            runCatching {
                progressLiveData.postValue(ProgressState.LOADING)
                apiService.getRandomJoke(firstName, lastName).value
            }.onSuccess {
                insertJoke(it)
                progressLiveData.postValue(ProgressState.SUCCESS)
                Log.d(TAG, "jokesLiveData onSuccess")
            }.onFailure {
                progressLiveData.postValue(ProgressState.ERROR)
                Log.d(TAG, "jokesLiveData onFailure")
            }
        }
    }

    private suspend fun insertJoke(jokeEntity: JokeEntity) {
        jokeDao.insertAll(jokeEntity)
    }

    override fun getProgressLiveData(): LiveData<ProgressState> {
        return progressLiveData
    }

    override fun getJokeLiveData(): LiveData<List<String>> = jokeDao.getAll()

}