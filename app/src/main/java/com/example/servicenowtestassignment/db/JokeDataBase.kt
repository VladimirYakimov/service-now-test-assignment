package com.example.servicenowtestassignment.db

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [JokeEntity::class], version = 1, exportSchema = false)
abstract class JokeDataBase : RoomDatabase() {
    abstract fun jokeDao(): JokeDao
}