package com.example.servicenowtestassignment.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "joke_table")
data class JokeEntity(
    @PrimaryKey(autoGenerate = true) val jid: Int = 0,
    @ColumnInfo(name = "id") val id: Int,
    @ColumnInfo(name = "joke") val joke: String
)