package com.example.servicenowtestassignment.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface JokeDao {

    @Query("SELECT joke FROM joke_table")
    fun getAll(): LiveData<List<String>>

    @Insert
    suspend fun insertAll(vararg joke: JokeEntity)

}