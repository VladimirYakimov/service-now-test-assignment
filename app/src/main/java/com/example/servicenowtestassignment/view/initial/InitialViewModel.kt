package com.example.servicenowtestassignment.view.initial

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.servicenowtestassignment.initial.InitialRepository
import javax.inject.Inject

class InitialViewModel @Inject constructor(private val repository: InitialRepository) :
    ViewModel() {
    private var firstNameLiveData = MutableLiveData<String>()
    private var lastNameLiveData = MutableLiveData<String>()

    init {
        firstNameLiveData.value = repository.getFirstName()
        lastNameLiveData.value = repository.getLastName()
    }

    fun getFirstNameLiveData(): LiveData<String> = firstNameLiveData

    fun getLastNameLiveData(): LiveData<String> = lastNameLiveData

    fun setFirstName(firstName: String) {
        repository.setFirstName(firstName)
        firstNameLiveData.value = firstName
    }

    fun setLastName(lastName: String) {
        repository.setLastName(lastName)
        lastNameLiveData.value = lastName
    }
}