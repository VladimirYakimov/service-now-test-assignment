package com.example.servicenowtestassignment.view.mainview

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager.widget.ViewPager
import androidx.viewpager2.widget.ViewPager2
import com.example.servicenowtestassignment.R
import com.example.servicenowtestassignment.view.initial.InitialActivity
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject


class MainActivity : DaggerAppCompatActivity() {
    private val TAG = "MainActivity"

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var mainViewModel: MainViewModel

    lateinit var mainRecyclerAdapter: MainRecyclerViewAdapter

    lateinit var onPageChangeCallback: ViewPager2.OnPageChangeCallback

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
    }

    private fun init() {
        initOnPageChangeCallback()
        mainRecyclerAdapter = MainRecyclerViewAdapter()
        mainViewModel = ViewModelProvider(this, viewModelFactory)[MainViewModel::class.java]
        mainViewModel.getJokesLiveData().observe(this, {
            hideProgressBar()
            mainRecyclerAdapter.updateJokeList(it)
            mainViewPager.currentItem = mainRecyclerAdapter.itemCount
        })
        mainViewPager.orientation = ViewPager2.ORIENTATION_HORIZONTAL
        mainViewPager.adapter = mainRecyclerAdapter
        mainViewPager.registerOnPageChangeCallback(onPageChangeCallback)

        mainViewModel.getProgressLiveData().observe(this, {
            when (it) {
                ProgressState.LOADING -> showProgreeBar()
                ProgressState.SUCCESS -> hideProgressBar()
                ProgressState.ERROR -> {
                    hideProgressBar()
                    Toast.makeText(this, R.string.oops, Toast.LENGTH_LONG).show()
                    if (mainRecyclerAdapter.itemCount == 0) {
                        refreshButton.visibility = View.VISIBLE
                    }
                }
                else -> hideProgressBar()
            }
        })

        if (mainViewModel.ifNeedOpenInitialActivity()) {
            openInitialActivity()
        }
        refreshButton.setOnClickListener {
            getNextJoke()
            refreshButton.visibility = View.GONE
        }
    }

    private fun initOnPageChangeCallback() {
        onPageChangeCallback = object : ViewPager2.OnPageChangeCallback() {
            private val thresholdOffset: Float = 0.5f
            var scrollStarted: Boolean = false
            var checkDirection: Boolean = false

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
                if (checkDirection) {
                    if (thresholdOffset > positionOffset && (position == mainRecyclerAdapter.itemCount - 1)) {
                        Log.i(TAG, "going forward, position = $position")
                        if (hasNetworkAvailable()) {
                            getNextJoke()
                        } else {
                            Toast.makeText(
                                this@MainActivity,
                                R.string.no_internet_connection,
                                Toast.LENGTH_LONG
                            ).show()
                        }
                    }
                    checkDirection = false
                }
            }

            override fun onPageScrollStateChanged(state: Int) {
                if (!scrollStarted && state == ViewPager.SCROLL_STATE_DRAGGING) {
                    scrollStarted = true
                    checkDirection = true
                } else {
                    scrollStarted = false
                }
            }

            override fun onPageSelected(position: Int) {
                Log.d(TAG, "onPageSelected position = $position")
                super.onPageSelected(position)
            }

        }
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.changeNameMenu -> {
                openInitialActivity()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun openInitialActivity() {
        val intent = Intent(this@MainActivity, InitialActivity::class.java) // (1) (2)
        startActivity(intent)
    }

    private fun getNextJoke() {

        mainViewModel.getJoke()
    }

    private fun showProgreeBar() {
        progressCircular.visibility = View.VISIBLE
    }

    private fun hideProgressBar() {
        progressCircular.visibility = View.GONE
    }

    private fun hasNetworkAvailable(): Boolean {
        val service = Context.CONNECTIVITY_SERVICE
        val manager = getSystemService(service) as ConnectivityManager?
        val network = manager?.activeNetworkInfo
        Log.d(TAG, "hasNetworkAvailable: ${(network != null)}")
        return (network?.isConnected) ?: false
    }
}