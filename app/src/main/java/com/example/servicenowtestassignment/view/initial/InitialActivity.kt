package com.example.servicenowtestassignment.view.initial

import android.os.Bundle
import androidx.core.widget.doAfterTextChanged
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.servicenowtestassignment.R
import com.google.android.material.textfield.TextInputLayout
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_initial.*
import javax.inject.Inject

class InitialActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var initialViewModel: InitialViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_initial)
        init()
    }

    private fun init() {
        initialViewModel = ViewModelProvider(this, viewModelFactory)[InitialViewModel::class.java]
        submitButton.setOnClickListener { submit() }
        firstNameTextInputLayout.editText?.doAfterTextChanged {
            submitValidation()
        }
        lastNameTextInputLayout.editText?.doAfterTextChanged {
            submitValidation()
        }
        initialViewModel.getFirstNameLiveData().observe(this, Observer {
            firstNameTextInputLayout.editText?.setText(it)
        })
        initialViewModel.getLastNameLiveData().observe(this, {
            lastNameTextInputLayout.editText?.setText(it)
        })
    }

    override fun onBackPressed() {}

    override fun onResume() {
        super.onResume()
        submitValidation()
    }

    private fun submitValidation() {
        submitButton.isEnabled =
            (!firstNameTextInputLayout.isTextNullOrEmpty() && !lastNameTextInputLayout.isTextNullOrEmpty())
    }

    private fun submit() {
        initialViewModel.setFirstName(firstNameTextInputLayout.editText?.text.toString())
        initialViewModel.setLastName(lastNameTextInputLayout.editText?.text.toString())
        finish()
    }

    private fun TextInputLayout.isTextNullOrEmpty(): Boolean {
        return this.editText?.text.isNullOrEmpty()
    }

}