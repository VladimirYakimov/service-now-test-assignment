package com.example.servicenowtestassignment.view.mainview

enum class ProgressState {
    LOADING, SUCCESS, ERROR
}