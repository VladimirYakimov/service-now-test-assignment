package com.example.servicenowtestassignment.view.mainview

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.text.HtmlCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.servicenowtestassignment.R

class MainRecyclerViewAdapter :
    RecyclerView.Adapter<MainRecyclerViewAdapter.RecyclerViewHolder>() {

    private var jokeList: MutableList<String> = mutableListOf()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        return RecyclerViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.joke_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        holder.jokeTextView.text =
            HtmlCompat.fromHtml(jokeList[position], HtmlCompat.FROM_HTML_MODE_LEGACY)
    }

    override fun getItemCount(): Int {
        return jokeList.size
    }

    fun updateJokeList(jokeList: List<String>) {
        this.jokeList = jokeList as MutableList<String>
        notifyDataSetChanged()

    }

    class RecyclerViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        val jokeTextView = v.findViewById<TextView>(R.id.jokeTextView)
    }
}