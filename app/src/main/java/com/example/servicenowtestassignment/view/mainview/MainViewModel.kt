package com.example.servicenowtestassignment.view.mainview

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.servicenowtestassignment.initial.InitialRepository
import com.example.servicenowtestassignment.network.api.ApiRepository
import kotlinx.coroutines.launch
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val apiRepository: ApiRepository,
    private var initialRepository: InitialRepository,
) : ViewModel() {

    private val jokesLiveData: LiveData<List<String>>
    private val progressLiveData: LiveData<ProgressState>

    init {
        jokesLiveData = apiRepository.getJokeLiveData()
        progressLiveData = apiRepository.getProgressLiveData()
        if (jokesLiveData.value.isNullOrEmpty()) {
            getJoke()
        }
    }

    fun getJokesLiveData(): LiveData<List<String>> {
        return jokesLiveData
    }

    fun getProgressLiveData(): LiveData<ProgressState> {
        return progressLiveData
    }

    fun getJoke() {
        viewModelScope.launch {
            apiRepository.getJoke(
                initialRepository.getFirstName(),
                initialRepository.getLastName()
            )
        }
    }

    fun ifNeedOpenInitialActivity(): Boolean {
        return initialRepository.getFirstName().isEmpty() || initialRepository.getLastName()
            .isEmpty()
    }

}