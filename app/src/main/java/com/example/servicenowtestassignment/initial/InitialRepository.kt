package com.example.servicenowtestassignment.initial

import android.content.SharedPreferences
import com.example.servicenowtestassignment.injection.modules.FIRST_NAME_KEY
import com.example.servicenowtestassignment.injection.modules.LAST_NAME_KEY
import javax.inject.Inject

class InitialRepository @Inject constructor() {

    @Inject
    lateinit var sharedPreferences: SharedPreferences


    fun setFirstName(firstName: String) =
        sharedPreferences.edit().putString(FIRST_NAME_KEY, firstName).apply()


    fun getFirstName(): String = sharedPreferences.getString(FIRST_NAME_KEY, "") ?: ""


    fun setLastName(lastName: String) =
        sharedPreferences.edit().putString(LAST_NAME_KEY, lastName).apply()

    fun getLastName(): String = sharedPreferences.getString(LAST_NAME_KEY, "") ?: ""

}